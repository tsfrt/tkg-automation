#! /bin/bash

DEST_IP=$1
PATH_TO_KEY=$2

WORKING_DIR=$(dirname "$0")
rm /tmp/deploy.tar
if [ "$(uname)" == "Darwin" ]; then
  tar -cfz --exclude=.git --exclude=.terraform --exclude=tkg-images-v1.1.3 -f /tmp/deploy.tar $WORKING_DIR 1> /dev/null
else
  tar cfz /tmp/deploy.tar $WORKING_DIR 1> /dev/null
fi
scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i $PATH_TO_KEY /tmp/deploy.tar ubuntu@$DEST_IP:~/
ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i $PATH_TO_KEY ubuntu@$DEST_IP "tar xf deploy.tar && chmod +x *.sh && chmod +x dependencies/*.sh && rm deploy.tar"
