#! /bin/bash

if [[ -z $1 ]]; then
    echo "Error: missing arg for env export file"
    echo "Usage: ./deploy-mgmt-cluster.sh <ENV_EXPORT_FILE>"
    exit 1
elif [[ ! -f $1 ]]; then
    echo "Error: env export file not found"
    exit 1
fi
set -ue #abort on error or if vars not set 

source $1

WORKING_DIR=$(dirname "$0")

# Substitute the variables in the containerd config.toml file, base64 encode and set as env
export TKG_CUSTOM_IMAGE_REPOSITORY_FQDN=$(echo ${TKG_CUSTOM_IMAGE_REPOSITORY} | cut -d'/' -f1)
export AIRGAPPED_B64ENCODED_TOML=$(envsubst < ${WORKING_DIR}/templates/containerd-config.toml | base64 -w0)

#Validate tkg cli is present and supported version
if ! which tkg; then
    echo "Error: tkg cli not found"
    exit 1
elif [[ "$(tkg version -o json | jq -r '.Version')" != 'v1.1.3' ]]; then
    echo "Error: This deployment bootstrap only supports TKG v1.1.3"
    exit 1
fi

# use tkg cli to populate ~/.tkg dir
tkg get mc

# Add additional TKG plans for IAAS
if [[ "${IAAS}" == "aws" ]]; then
    cp -r ${WORKING_DIR}/plans/aws/* ~/.tkg/providers/infrastructure-aws/
elif [[ "${IAAS}" == "vsphere" ]]; then
    cp -r ${WORKING_DIR}/plans/aws/* ~/.tkg/providers/infrastructure-vsphere/
fi

# write root ca for private registry into kind dir
echo ${AIRGAPPED_B64ENCODED_CA} | base64 -d > ${WORKING_DIR}/kind/registry-ca.crt

# inject root ca into kind node image and tag for private registry
docker build -t ${TKG_CUSTOM_IMAGE_REPOSITORY}/kind/node:v1.18.6_vmware.1 ${WORKING_DIR}/kind/

# remove cert file
rm -f ${WORKING_DIR}/kind/registry-ca.crt

tkg init --infrastructure ${IAAS} --name $MGMT_CLUSTER_NAME --plan $PLAN -v $VERBOSE

if [[ "${VAULT_ENABLED}" == "true" ]]; then
    encoded_conf=$(cat ~/.kube-tkg/config | base64 -w 0)
    vault kv put ${VAULT_KUBE_PATH} value=$encoded_conf
fi