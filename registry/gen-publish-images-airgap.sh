#!/usr/bin/env bash
# Copyright 2020 The TKG Contributors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
DIR=$( dirname "${0}" )

BOM_DIR=${HOME}/.tkg/bom

TKGTARDIR=tkg-images-$(tkg version -o json | jq -r '.Version')

getImageScript=${HOME}/pull-images.sh
pushImageScript=${HOME}/push-images.sh
imageListFile=${HOME}/imageList

echo '' > ${getImageScript}
echo '' > ${imageListFile}
echo '#!/usr/bin/env bash' >> ${getImageScript}
echo "TARDIR=\${HOME}/${TKGTARDIR}" >> ${getImageScript}
echo "mkdir \${TARDIR}" >> ${getImageScript}


echo '#!/usr/bin/env bash' >> ${pushImageScript}
echo 'if [ -z "${1}" ]; then' >> ${pushImageScript}
echo '    echo "missing custom image repository/project argument"' >> ${pushImageScript}
echo '    echo "e.g. $ push-images.sh myimagerepo.example.org"' >> ${pushImageScript}
echo '    echo "e.g. $ push-images.sh myimagerepo.example.org/tkg"' >> ${pushImageScript}
echo '    exit 1' >> ${pushImageScript}
echo 'else' >> ${pushImageScript}
echo "    customRegistry=\$(echo \$1 | sed 's/\//\\\\\//g')" >> ${pushImageScript}
echo 'fi' >> ${pushImageScript}


echo -n "Processing Bill of Materials files"
for TKG_BOM_FILE in "${BOM_DIR}"/*.yaml; do
    echo -n "."
    # Get actual image repository from BoM file
    actualImageRepository=$(yq r "${TKG_BOM_FILE}" imageConfig.imageRepository | tr -d '"')

    # Iterate through BoM file to create the complete Image name
    # and then pull, retag and push image to custom registry
    yq r "${TKG_BOM_FILE}" --tojson images | jq -c '.[]' | while read -r i; do
        # Get imagePath and imageTag
        imagePath=$(jq .imagePath <<<"$i" | tr -d '"')
        imageTag=$(jq .tag <<<"$i" | tr -d '"')

        # create complete image names
        echo "${actualImageRepository}|${actualImageRepository}/${imagePath}:${imageTag}" >> ${imageListFile}
    done
done
echo ''

echo "Processing unique images"
uniqueImageList=($(sort -u ${imageListFile}))
totalImages=${#uniqueImageList[@]}
startCount=1
for imageData in ${uniqueImageList[@]}; do
    actualRegistry=$(echo ${imageData} | cut -d'|' -f1)
    actualImage=$(echo ${imageData} | cut -d'|' -f2)
    image=$(echo ${actualImage} | cut -d'/' -f2-)
    #actualRegistry=$(echo ${actualImage} | cut -d'/' -f1)
    imageName=$(echo ${actualImage} | awk -F/ '{print $NF}' | tr ':' '-')

    echo "echo \"Processing image ${startCount} of ${totalImages}\"" >> ${getImageScript}
    echo "docker pull ${actualImage}" >> ${getImageScript}
    echo "docker save ${actualImage} -o \${TARDIR}/${imageName}.tar" >> ${getImageScript}
    echo "gzip \${TARDIR}/${imageName}.tar" >> ${getImageScript}
    echo "echo ${imageName}.tar.gz,${actualImage} >> \${TARDIR}/manifest.csv" >> ${getImageScript}
    echo "" >> ${getImageScript}

    echo "echo \"Processing image ${startCount} of ${totalImages}\"" >> ${pushImageScript}
    echo "cat ${imageName}.tar.gz | docker load" >> ${pushImageScript}
    echo "docker tag  ${actualImage} \$(echo ${actualImage} | sed \"s|${actualRegistry}|\${customRegistry}|\")" >> ${pushImageScript}
    echo "docker push \$(echo ${actualImage} | sed \"s|${actualRegistry}|\${customRegistry}|\")" >> ${pushImageScript}
    echo "" >> ${pushImageScript}
    ((startCount++))
done

echo "echo \"$(base64 ~/push-images.sh)\" | base64 -d > \${TARDIR}/push-images.sh" >> ${getImageScript}
echo "chmod +x \${TARDIR}/push-images.sh" >> ${getImageScript}
echo "echo \"Push image script file created: \${TARDIR}/push-images.sh\"" >> ${getImageScript}
echo "echo \"Use the following command to tar up the pulled/saved TKG images:\"" >> ${getImageScript}
echo "echo \"  tar -zcf \${TARDIR}.tgz \${TARDIR}\"" >> ${getImageScript}

rm -f ${pushImageScript} ${imageListFile}
chmod +x ${getImageScript}

echo "Pull image script file created: ${getImageScript}"
