# VPC
output "ag_vpc_id" {
  description = "The ID of the VPC"
  value       = module.air-gapped-vpc.vpc_id
}

output "connected_vpc_id" {
  description = "The ID of the VPC"
  value       = module.tkg-connected-vpc.vpc_id
}

output "tkg-ag-jumpbox" {
  value       = module.tkg-ag-jumpbox.private_ip
}

output "tkg-connected-jumpbox" {
  value       = module.tkg-connected-jumpbox.public_ip
}


# Subnets
output "private_subnet1" {
  description = "List of IDs of private subnets"
  value       = module.air-gapped-vpc.private_subnets[0]
}

output "private_subnet2" {
  description = "List of IDs of private subnets"
  value       = module.air-gapped-vpc.private_subnets[1]
}

output "public_subnets" {
  description = "List of IDs of public subnets"
  value       = module.air-gapped-vpc.public_subnets
}

# VPC endpoints
output "vpc_endpoint_ssm_id" {
  description = "The ID of VPC endpoint for SSM"
  value       = module.air-gapped-vpc.vpc_endpoint_ssm_id
}

output "vpc_endpoint_ssm_network_interface_ids" {
  description = "One or more network interfaces for the VPC Endpoint for SSM."
  value       = module.air-gapped-vpc.vpc_endpoint_ssm_network_interface_ids
}

output "vpc_endpoint_ssm_dns_entry" {
  description = "The DNS entries for the VPC Endpoint for SSM."
  value       = module.air-gapped-vpc.vpc_endpoint_ssm_dns_entry
}




