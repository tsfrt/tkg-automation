variable "VPC_NAME" {
  type    = string
  default = "tkg-ag-demo"
}

variable "AWS_REGION" {
  type    = string
  default = "us-east-1"
}

variable "AWS_SSH_KEY_NAME" {
  type    = string
  default = "ag-tkg"
}

variable "AWS_VPC_CIDR" {
  type    = string
  default = "10.0.0.0/16"
}

variable "AWS_NODE_AZ" {
  type    = string
  default = "us-east-1a"
}

variable "AWS_NODE_AZ_2" {
  type    = string
  default = "us-east-1b"
}

variable "AWS_PRIVATE_NODE_CIDR" {
  type    = string
  default = "10.0.50.0/24"
}

variable "AWS_PRIVATE_NODE_CIDR_2" {
  type    = string
  default = "10.0.51.0/24"
}

variable "TKG_CUSTOM_IMAGE_REPOSITORY_FQDN" {
  type    = string
  default = "tkg-ag-jumpbox"
}

provider "aws" {
  region = var.AWS_REGION
}

module "air-gapped-vpc" {
  source = "terraform-aws-modules/vpc/aws"
  name = var.VPC_NAME
  cidr = var.AWS_VPC_CIDR

  azs             = list( var.AWS_NODE_AZ, var.AWS_NODE_AZ_2)
  private_subnets = list( var.AWS_PRIVATE_NODE_CIDR, var.AWS_PRIVATE_NODE_CIDR_2)
  
  enable_dns_hostnames = true
  enable_dns_support   = true

  enable_nat_gateway = false
  enable_vpn_gateway = false
  #enable_s3_endpoint = true

  manage_default_security_group  = true
  default_security_group_ingress = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = "0.0.0.0/0"
    }
  ]
  default_security_group_egress  = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = "0.0.0.0/0"
    }
  ]

  enable_elasticloadbalancing_endpoint              = true
  elasticloadbalancing_endpoint_private_dns_enabled = true
  elasticloadbalancing_endpoint_security_group_ids  = [module.air-gapped-vpc.default_security_group_id]

  enable_ssm_endpoint              = true
  ssm_endpoint_private_dns_enabled = true
  ssm_endpoint_security_group_ids  = [module.air-gapped-vpc.default_security_group_id]


  enable_ssmmessages_endpoint              = true
  ssmmessages_endpoint_private_dns_enabled = true
  ssmmessages_endpoint_security_group_ids  = [module.air-gapped-vpc.default_security_group_id]


  enable_ec2_endpoint              = true
  ec2_endpoint_private_dns_enabled = true
  ec2_endpoint_security_group_ids  = [module.air-gapped-vpc.default_security_group_id]


  enable_ec2messages_endpoint              = true
  ec2messages_endpoint_private_dns_enabled = true
  ec2messages_endpoint_security_group_ids  = [module.air-gapped-vpc.default_security_group_id]

  enable_sts_endpoint              = true
  sts_endpoint_private_dns_enabled = true
  sts_endpoint_security_group_ids  = [module.air-gapped-vpc.default_security_group_id]
  
  enable_secretsmanager_endpoint              = true
  secretsmanager_endpoint_private_dns_enabled = true
  secretsmanager_endpoint_security_group_ids  = [module.air-gapped-vpc.default_security_group_id]

  enable_ecr_api_endpoint              = true
  ecr_api_endpoint_private_dns_enabled = true
  ecr_api_endpoint_security_group_ids  = [module.air-gapped-vpc.default_security_group_id]

  enable_ecr_dkr_endpoint              = true
  ecr_dkr_endpoint_private_dns_enabled = true
  ecr_dkr_endpoint_security_group_ids  = [module.air-gapped-vpc.default_security_group_id]

tags = {
    Terraform   = "true"
    Environment = "tkg-demo"
  }

vpc_tags = {
    Name = var.VPC_NAME
  }
}

data "aws_ami" "ubuntu" {
    most_recent = true

    filter {
        name   = "name"
        values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*"]
    }

    filter {
        name   = "virtualization-type"
        values = ["hvm"]
    }

    owners = ["099720109477"] # Canonical
}

module "tkg-ag-jumpbox" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "2.12.0"

  name           = "tkg-ag-jumpbox"
  instance_count = 1

  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t2.medium"
  vpc_security_group_ids = [module.air-gapped-vpc.default_security_group_id]
  subnet_id              = module.air-gapped-vpc.private_subnets[0]
  key_name               = var.AWS_SSH_KEY_NAME
  
  root_block_device = [
    {
      volume_type = "gp2"
      volume_size = 40
    },
  ]

  tags = {
    Terraform   = "true"
    Environment = "tkg-demo"
  }
}

variable "CONNECTED_VPC_NAME" {
  type    = string
  default = "tkg-connected-vpc"
}

variable "CONNECTED_VPC_CIDR" {
  type    = string
  default = "10.50.0.0/16"
}

variable "CONNECTED_SUBNET_VPC_CIDR" {
  type    = string
  default = "10.50.0.0/24"
}


module "tkg-connected-vpc" {
  source = "terraform-aws-modules/vpc/aws"
  name = join("-",[var.CONNECTED_VPC_NAME, var.VPC_NAME])
  cidr = var.CONNECTED_VPC_CIDR

  azs            = list( var.AWS_NODE_AZ)
  public_subnets = list( var.CONNECTED_SUBNET_VPC_CIDR)
  
  enable_dns_hostnames = true
  enable_dns_support   = true
  create_igw           = true
  enable_nat_gateway   = false
  enable_vpn_gateway   = false

  manage_default_security_group  = true
  default_security_group_ingress = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = "0.0.0.0/0"
    }
  ]
  default_security_group_egress  = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = "0.0.0.0/0"
    }
  ]

tags = {
    Terraform   = "true"
    Environment = "tkg-demo"
  }

vpc_tags = {
    Name = var.CONNECTED_VPC_NAME
  }
}

module "tkg-connected-jumpbox" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "2.12.0"

  name           = "tkg-connected-jumpbox"
  instance_count = 1

  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t2.medium"
  vpc_security_group_ids = [module.tkg-connected-vpc.default_security_group_id]
  subnet_id              = module.tkg-connected-vpc.public_subnets[0]
  key_name               = var.AWS_SSH_KEY_NAME
  
  root_block_device = [
    {
      volume_type = "gp2"
      volume_size = 40
    },
  ]

  tags = {
    Terraform   = "true"
    Environment = "tkg-demo"
  }
}

resource "aws_vpc_peering_connection" "tkg-peering" {
  vpc_id      = module.tkg-connected-vpc.vpc_id
  peer_vpc_id = module.air-gapped-vpc.vpc_id
  auto_accept = true
}

resource "aws_route" "connected-vpc-route-sn1" {
  route_table_id            = module.tkg-connected-vpc.public_route_table_ids[0]
  destination_cidr_block    = var.AWS_PRIVATE_NODE_CIDR
  vpc_peering_connection_id = aws_vpc_peering_connection.tkg-peering.id
}

resource "aws_route" "connected-vpc-route-sn2" {
  route_table_id            = module.tkg-connected-vpc.public_route_table_ids[0]
  destination_cidr_block    = var.AWS_PRIVATE_NODE_CIDR_2
  vpc_peering_connection_id = aws_vpc_peering_connection.tkg-peering.id
}

resource "aws_route" "air-gapped-vpc-route" {
  route_table_id            = module.air-gapped-vpc.private_route_table_ids[0]
  destination_cidr_block    = var.CONNECTED_SUBNET_VPC_CIDR
  vpc_peering_connection_id = aws_vpc_peering_connection.tkg-peering.id
}
