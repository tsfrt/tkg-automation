#! /bin/bash

if [[ -z $1 ]]; then
    echo "Error: missing arg for env export file"
    echo "Usage: ./deploy-mgmt-cluster.sh <ENV_EXPORT_FILE>"
    exit 1
elif [[ ! -f $1 ]]; then
    echo "Error: env export file not found"
    exit 1
fi
set -ue #abort on error or if vars not set 

source $1

WORKING_DIR=$(dirname "$0")

source ${WORKING_DIR}/terraform/env.sh
terraform init -input=false ${WORKING_DIR}/terraform
terraform plan -out="$1-plan" -input=false ${WORKING_DIR}/terraform
terraform apply -input=false ${WORKING_DIR}/terraform


ps_1=$(terraform output private_subnet1)
ps_2=$(terraform output private_subnet1)
vpc_id=$(terraform output ag_vpc_id)

sed  "s/export AWS_PRIVATE_SUBNET_ID=.*/export AWS_PRIVATE_SUBNET_ID=$ps_1/g" $1 >"$1-temp" 
sed  "s/export AWS_PRIVATE_SUBNET_ID_2=.*/export AWS_PRIVATE_SUBNET_ID_2=$ps_2/g" "$1-temp" > "$1-temp2" 
sed  "s/export AWS_VPC_ID=.*/export AWS_VPC_ID=$vpc_id/g" "$1-temp2" > "$1-temp3" 

cp $1 $1-backup
cp "$1-temp3" $1
rm "$1-temp"*


echo "Please take note of your connected jump boxes IP address info"

echo ">>>>>>>>>>>>" $(terraform output tkg-connected-jumpbox) 


echo "You will also need to know the ip address of your airgapped jump box"

echo ">>>>>>>>>>>>" $(terraform output tkg-ag-jumpbox)

