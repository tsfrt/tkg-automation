# TKG MGMT deployment script
## Purpose: 
This script (deploy-mgmt-cluster.sh) installs a TKG v1.1.3 management cluster.  
This repo represents a point in time deploy and will evolve as newer versions of TKG are rleased/integrated.  
It uses a bootstrap single node kubernetes cluster running on top of docker (KinD) to provision the management cluster in the IAAS.   
This automation is writen to facilitate deployment into offline/airgapped environments.  

# AWS Airgap Deployment
### ENV Prereq:

<details><summary>CloudFormation for IAM profile</summary>
<a href=https://docs.vmware.com/en/VMware-Tanzu-Kubernetes-Grid/1.1/vmware-tanzu-kubernetes-grid-11/GUID-install-tkg-aws.html#clusterawsadm>VMware Documentation</a>

```bash
export AWS_ACCESS_KEY_ID=aws_access_key
export AWS_SECRET_ACCESS_KEY=aws_access_key_secret
# if needed 
export AWS_SESSION_TOKEN=aws_session_token

export AWS_REGION=uaws_region

clusterawsadm alpha bootstrap create-stack
```
</details>

<details><summary>AWS base64 encoded credentials for IAM user created in CloudFormation</summary>
<a href=https://docs.vmware.com/en/VMware-Tanzu-Kubernetes-Grid/1.1/vmware-tanzu-kubernetes-grid-11/GUID-install-tkg-aws.html#set-credentials-capi>VMware Documentation</a>

```bash
# Set a new environment variable for the IAM user credentials
export AWS_CREDENTIALS=$(aws iam create-access-key --user-name bootstrapper.cluster-api-provider-aws.sigs.k8s.io --output json)

# Replace the environment variable that you created for your AWS access key ID.

export AWS_ACCESS_KEY_ID=$(echo $AWS_CREDENTIALS | jq .AccessKey.AccessKeyId -r)

# Replace the environment variable that you created for your secret access key.

export AWS_SECRET_ACCESS_KEY=$(echo $AWS_CREDENTIALS | jq .AccessKey.SecretAccessKey -r)

# Set a new environment variable to encode the IAM user AWS credentials.

export AWS_B64ENCODED_CREDENTIALS=$(clusterawsadm alpha bootstrap encode-aws-credentials)
```

</details>

<details><summary>AWS SSH key pair</summary>
<a href=https://docs.vmware.com/en/VMware-Tanzu-Kubernetes-Grid/1.1/vmware-tanzu-kubernetes-grid-11/GUID-install-tkg-aws.html#register-ssh>VMware Documentation</a>

If you do not already have an SSH key pair, you can use the AWS CLI to create one, by performing the steps below.

```bash
aws ec2 create-key-pair --key-name <KEY_PAIR_NAME> --output json | jq .KeyMaterial -r > <KEY_PAIR_NAME>.pem

```

</details>

<details><summary>Container registery with required images and tls endpoint</summary>
<a href=https://docs.vmware.com/en/VMware-Tanzu-Kubernetes-Grid/1.1/vmware-tanzu-kubernetes-grid-11/GUID-install-tkg-airgapped-environments.html#tkg-112>VMware Documentation</a>

This is a modified version of the script found in the VMware documentation to facilitate pulling the images from an internet connected jumpbox and pushing the images from and airgapped jumpbox.

```bash
# generate the ~/.tkg directory structure so the bom files are present
tkg get mc

# generate the pull script from an interned connected jumpbox
./registry/gen-publish-images-airgap.sh

# pull th images to the internet connected jumpbox
~/pull-images.sh

# tar and gzip the directory containing the images, then move the tgz file to the airgapped jumpbox
tar -zcf ~/tkg-images-$(tkg version -o json | jq -r '.Version').tgz ~/tkg-images-$(tkg version -o json | jq -r '.Version')


# from the airgapped jumpbox
tar -zxf tkg-images-$(tkg version -o json | jq -r '.Version').tgz -C ~

# cd into the unpacked image files directory
cd ~/tkg-images-$(tkg version -o json | jq -r '.Version')

# Set an env for the registry
# e.g. generic registry myimagerepo.example.org
# e.g. harbor registry myimagerepo.example.org/<project_name> (the lowest level to store an image in harbor is a project)
export TKG_CUSTOM_IMAGE_REPOSITORY=<registry_fqdn>


# login to the private/airgapped registry
docker login ${TKG_CUSTOM_IMAGE_REPOSITORY}

# push the images to the airgapped regisry
./push-images.sh ${TKG_CUSTOM_IMAGE_REPOSITORY}

```

</details>

<details><summary>Root CA for PATH tls cert</summary>
Base64 encode the root ca or self signed certificate pem file for the airgapped registry and store in the environment variables file

```bash
cat <PEM_FILE> | base64 -w0
# store the base64 string as the value to AIRGAPPED_B64ENCODED_CA
```

</details>

### jumpbox Prereq:
- tkg == v1.1.3 cli in $PATH
- jq >= 1.6 cli in $PATH
- yq >= 3.4.0 cli in $PATH
- docker >= 18.09.7 in $PATH

### Optional:
- Vault server /vault cli in $PATH

### Usage:
1) Populate an environment variables file using the template
```bash
$ cp ./templates/aws-airgap-mgmt-config.tpl ~/aws-tkg-env
$ vim ~/aws-tkg-env
# Add all of the reqired metadata for the deployment
```


1) Ensure you have the `tkg` and `jq` cli's in a location that can be called from the script. 
```
./deploy-mgmt-cluster.sh <config-template>
```
```
./deploy-mgmt-cluster.sh ~/aws-tkg-env 