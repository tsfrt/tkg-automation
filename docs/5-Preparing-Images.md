# Preparing Install Images

We need to bundle all of the required container images for the airgapped install.  If you already have a registry in your environment, your path may stray here.  We have included a basic registry image that can be used as a temporary starting point.

We need to initiliaze the TKG CLI in order to get a list of the required container images.  

```bsash
tkg get mc
```

We also need to settle on the DNS name for our container registry.  We specified the name in our config earlier, but we need to set it as an env variable now.  Make sure you use a name you will be able to create a DNS record for later.

```bash
source <the config file you setup earlier with TKG_CUSTOM_IMAGE_REPOSITORY>

#to confirm
echo $TKG_CUSTOM_IMAGE_REPOSITORY
```

Now we are going to build up bundle of required images by running:

```bash
sudo ./registry/gen-publish-images-airgap.sh
```
This script will output a path to another script called pull-imags.sh. Upon running, you should be seeing images pulled down one by one.  This script will take ~5 mins to run, take a break.

```bash
#Pull image script file created: /home/ubuntu/pull-images.sh

sudo /home/ubuntu/pull-images.sh
```

You should see `Processing image 93 of 93` at the end of the script.  Now we are going to tar the images up.

```bash
sudo tar -zcf ~/tkg-images-$(tkg version -o json | jq -r '.Version').tgz ~/tkg-images-$(tkg version -o json | jq -r '.Version')
```

We now have all our images bundled in `tkg-images-v1.1.3.tgz'. Now we are going to move over to our air gapped environment.  Recall that we got the private ip of the air gapped jumpbox from Terraform.

```Bash
./deploy-env.sh <private ip of airgapped jumpbox> <private key for jumpbox>

ssh -i <private key for jumpbox> ubuntu@<private ip of airgapped jumpbox>
```



