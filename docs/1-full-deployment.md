# Full Deploy

1. Download tkg cli - copy it into tkg folder in project structure

1. Clone project to local machine

2. fill out configuration, stick with defaults unless you need

3. run aws configure

# If you do not have a VPC to run TKG in and want to create a private VPC with a public jumpbox start with running 
3. run create-tkg-aws-env.sh

# Now you have an environment to deploy TKG in.  
4. identify the ip of the internet connected jumpbox and run 
deploy-env.sh <jump box ip>

#Now we have staged our install o

5. ssh into the jumpbox with the ip you identified earlier

6. run ./install-connected-depends.sh

7. work through aws IAM cloud forms

8.  create a key pair if you need
aws ec2 create-key-pair --key-name ag-tkg --output json | jq .KeyMaterial -r > ag-tkg.pem

8. setup tkg cli that you downloaded

chmod +x <cli binary>
chmod +x <cli binary>
sudo cp <cli binary>  /usr/local/bin/tkg

run tkg get mc to init

9. Determine what registry you are going to use
set the host name 
export TKG_CUSTOM_IMAGE_REPOSITORY="tkg-airgap-install.tkg-demo.info"

(we have included a vanilla docker registry with instructions if you need to stand up a temporary registry for the install)
(remember that you will need dns setup for your registry if you set it up on your own)
(you will need the CA certs for your registry if it is already in place)

10 run 
./registry/gen-publish-images-airgap.sh
(this will take awhile)
sudo ~/.pull-images.sh
sudo tar -zcf ~/tkg-images-$(tkg version -o json | jq -r '.Version').tgz ~/tkg-images-$(tkg version -o json | jq -r '.Version')

11. identify your air gapped jump box ip

run 
deploy-env.sh <private jump box ip> <path to key>

install-ag-depends.sh

sudo docker load -i registry/registry-image.tar 
sudo docker run -d -p 80:5000 --restart=always --name registry registry:2

setup dns based on your 
export TKG_CUSTOM_IMAGE_REPOSITORY="tkg-airgap-install.tkg-demo.info"

I am using route53
-create a private tkg hosted zone
-add the airgapped vpc to it
-add ip of private jump box that we will use as temporary registry for install

on private jumpbox, verify dns resouloation

-dig $TKG_CUSTOM_IMAGE_REPOSITORY
