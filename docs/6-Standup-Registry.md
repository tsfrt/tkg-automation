# Standing Up an Install Registry

We have now deployed our project to the airgapped environment.  

Below are instructions on setting up a temporary registry for our install.  If you already have a registry, please check out the CA cert configuration below, but you will not need to run through the following steps.

```bash
source <config file>

#this will start docker
./install-ag-depends.sh

#load the registry image into docker
sudo docker load -i registry/registry-image.tar

#run the registry
sudo docker run -d -p 80:5000 --restart=always --name registry registry:2

#confirm the container is running 
sudo docker ps
```
We now have a working container registry.  This is just meant to support the install, so we are keeping it very simple.

Now we need create a DNS entry for our jumpbox that maps to our registry name that is specified in TKG_CUSTOM_IMAGE_REPOSITORY.  In my environment, I use route53 to create a A record for the jump box IP address.  This process will vary based on your environment.

In addition to DNS, you will also need to configure CA certs with the registry.  If your environment has an existing container registry that is configured with certificates, place the CA certificate (or chain) in a PEM format at `kind/registry-ca.crt' within the project.  This will ensure that your cert gets included in the resulting cluster.

If you use the simple registry provided within the project, you can skip adding CA certs.  We do need to add an exclusion to our containerd configuration for the temporary registry.  See the configuration below.

```
#add the "containerdConfigPatches" section into your ~/.tkg/bom/bom-1.1.3+vmware.1.yaml file.  
#With your registry name populated in the placeholders sections below

...
kindKubeadmConfigSpec:
- 'kind: Cluster'
- 'apiVersion: kind.x-k8s.io/v1alpha4'
- 'containerdConfigPatches:'
- '- |-'
- '  [plugins."io.containerd.grpc.v1.cri".registry.mirrors."<TKG_CUSTOM_IMAGE_REPOSITORY>"]'
- '    endpoint = ["http://<TKG_CUSTOM_IMAGE_REPOSITORY>"]'
- 'kubeadmConfigPatches:'
- '- |'
- '  apiVersion: kubeadm.k8s.io/v1beta2'
- '  kind: ClusterConfiguration'
- '  imageRepository: registry.tkg.vmware.run'
- '  etcd:'
...
```

With our registry configured and DNS established we need to push our image bundle into the registry.


```bash
#untar the images
tar xvf tkg-images-v1.1.3.tgz

cd tkg-images-v1.1.3/

#push the images into our new repo
sudo ./push-images.sh ${TKG_CUSTOM_IMAGE_REPOSITORY}

```

You should see `Processing image 93 of 93` at the end of the script execution.


