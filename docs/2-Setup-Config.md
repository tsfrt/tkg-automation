# Setup Your Configuration

At this point we want to clone this project locally.

```bash
git clone https://gitlab.com/tsfrt/tkg-automation.git
```

There is a template in the templates folder named aws-airgap-mgmt-config.tpl.  Make a copy of the file in the root of the project with a name that makes sense to you.

```bash
cp templates/aws-airgap-mgmt-config.tpl aws-airgap-mgmt-config-test1
```

Work through the configuration items and set them based on your environment


```bash
#AWS Var section
#dev-airgap, prod-airgap, oidc-airgap
export PLAN=
#numerical value
export VERBOSE=
#mgmt cluster name
export MGMT_CLUSTER_NAME=

#don't change this
export IAAS=aws
#select the region e.g. us-east-1
export AWS_REGION=
#select az, e.g. us-east-1a
export AWS_NODE_AZ=
#set your first private subnet if you are not using Terraform
export AWS_PRIVATE_SUBNET_ID=
#CIDR for first private subnet, this is required
export AWS_PRIVATE_NODE_CIDR=
#set your second private subnet id if you are not using Terraform
export AWS_PRIVATE_SUBNET_ID_2=
#set your second private subnet, this is required
export AWS_PRIVATE_NODE_CIDR_2=
#set the key name that will be used for your 
export AWS_SSH_KEY_NAME=
#this will create a bastion host, probably not needed here
export BASTION_HOST_ENABLED=false
#this is the cluster cidr 100.96.0.0/11
export CLUSTER_CIDR=
#machine size m5.xlarge
export CONTROL_PLANE_MACHINE_TYPE=
#machine size m5.xlarge
export NODE_MACHINE_TYPE=
#set the registry name
export TKG_CUSTOM_IMAGE_REPOSITORY=
#Credentials from the first step
export AWS_B64ENCODED_CREDENTIALS=
#This will be populated by the registry-ca.crt file later
export AIRGAPPED_B64ENCODED_CA=

#VAULT Section
export VAULT_ENABLED=false
export VAULT_ADDR=
export VAULT_TOKEN=
export VAULT_KUBE_PATH=
```
Here is an example of a bring your own VPC configuration.  Note that the subnet and VPC IDs are pre-populated.

```bash
#AWS Var section
export PLAN=dev-airgap
export VERBOSE=100
export MGMT_CLUSTER_NAME=air-gapped-mgmt1

export VPC_NAME=tkg-demo2
export IAAS=aws
export AWS_REGION=us-east-1
export AWS_NODE_AZ=us-east-1a
export AWS_VPC_ID=vpc-036a58414e2859af7
export AWS_PRIVATE_SUBNET_ID=subnet-0f451f49b8f4420d4
export AWS_PRIVATE_SUBNET_ID_2=subnet-0fb48e9d53c62f1e5
export AWS_SSH_KEY_NAME=ag-tkg
export AWS_VPC_ID=vpc-036a58414e2859af7
export AWS_VPC_CIDR=10.0.0.0/16
export AWS_PRIVATE_NODE_CIDR=10.0.50.0/24
export AWS_PRIVATE_NODE_CIDR_2=10.0.52.0/24
export BASTION_HOST_ENABLED=false
export CLUSTER_CIDR=100.96.0.0/11
export CONTROL_PLANE_MACHINE_TYPE=m5.xlarge
export NODE_MACHINE_TYPE=m5.xlarge
export TKG_CUSTOM_IMAGE_REPOSITORY=registry.tkg-demo.info:5000
export AWS_B64ENCODED_CREDENTIALS=<base 64 encoded creds from first step>
export AIRGAPPED_B64ENCODED_CA=<base 64 encoded ca certs>

#VAULT Section
export VAULT_ENABLED=false
export VAULT_ADDR=

```

With Terraform, just set the CIDR's as desired, the subnets and VPC IDs will be set by Terraform.

```bash

#AWS Var section
export PLAN=dev-airgap
export VERBOSE=100
export MGMT_CLUSTER_NAME=air-gapped-mgmt1

export VPC_NAME=tkg-demo2
export IAAS=aws
export AWS_REGION=us-east-1
export AWS_NODE_AZ=us-east-1a
export AWS_PRIVATE_SUBNET_ID=
export AWS_PRIVATE_SUBNET_ID_2=
export AWS_SSH_KEY_NAME=ag-tkg
export AWS_VPC_ID=
export AWS_VPC_CIDR=10.0.0.0/16
export AWS_PRIVATE_NODE_CIDR=10.0.50.0/24
export AWS_PRIVATE_NODE_CIDR_2=10.0.52.0/24
export BASTION_HOST_ENABLED=false
export CLUSTER_CIDR=100.96.0.0/11
export CONTROL_PLANE_MACHINE_TYPE=m5.xlarge
export NODE_MACHINE_TYPE=m5.xlarge
export TKG_CUSTOM_IMAGE_REPOSITORY=registry.tkg-demo.info
export AWS_B64ENCODED_CREDENTIALS=<base 64 encoded creds from first step>
export AIRGAPPED_B64ENCODED_CA=<base 64 encoded ca certs>

#VAULT Section
export VAULT_ENABLED=false
export VAULT_ADDR=

```
