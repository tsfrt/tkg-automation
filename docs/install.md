# Installing TKG

In the course of running through this install please ensure that you are careful with any credentials used for AWS.  The VPC and accompanying infrastructure is intended to be used as a POC only. The provisioned infrastructure is meant to aproximate an air gapped scenario and enable users to try out TKG.


- [Setup Your Configuration](https://gitlab.com/tsfrt/tkg-automation/-/blob/master/docs/install.md#setup-your-configuration)
  - [Bring Your Own VPC](https://gitlab.com/tsfrt/tkg-automation/-/blob/master/docs/install.md#bring-your-own-vpc)
  - [Using Terraform](https://gitlab.com/tsfrt/tkg-automation/-/blob/master/docs/install.md#use-terraform-to-create-an-environment)
- [Running Terraform](https://gitlab.com/tsfrt/tkg-automation/-/blob/master/docs/install.md#running-terraform)
  - [Jump Box Connectivity](https://gitlab.com/tsfrt/tkg-automation/-/blob/master/docs/install.md#take-note-of-your-jumpbox-ip-addresses)
- [TKG User](https://gitlab.com/tsfrt/tkg-automation/-/blob/master/docs/install.md#setting-iam-profile-and-user-credentials-for-tkg)
- [Staging Your Deployment](https://gitlab.com/tsfrt/tkg-automation/-/blob/master/docs/install.md#staging-your-deployment-on-the-connected-jumpbox)
  - [TKG CLI](https://gitlab.com/tsfrt/tkg-automation/-/blob/master/docs/install.md#make-sure-you-have-the-tkg-cli)
- [Preparing Install Image](https://gitlab.com/tsfrt/tkg-automation/-/blob/master/docs/install.md#preparing-install-images)
  - [Custom Image Registry](https://gitlab.com/tsfrt/tkg-automation/-/blob/master/docs/install.md#take-note-when-selecting-your-image-registry)
- [Standing Up Install Registry](https://gitlab.com/tsfrt/tkg-automation/-/blob/master/docs/install.md#standing-up-an-install-registry)
  - [CA Cert Considerations](https://gitlab.com/tsfrt/tkg-automation/-/blob/master/docs/install.md#ca-cert-considerations)
  - [Install Registry](https://gitlab.com/tsfrt/tkg-automation/-/blob/master/docs/install.md#use-the-install-registry)
- [Creating A Management Cluster](https://gitlab.com/tsfrt/tkg-automation/-/blob/master/docs/install.md#creating-a-management-cluster)

### Before getting started
- For the purposes of this install project, the assumption is made that you are running from within the root folder of this project for all the commands below unless otherwise specified.  
- This has been tested on mac os and ubuntu.  The provisioned jumpboxes created by Terraform are vanilla Ubuntu boxes.
- None of the infrastructure created with Terraform is intended to meet any rigorious security standard, this is inteded as a demo environment. 

## Setup Your Configuration

We need to clone a local copy of the TKG Project.

```bash
git clone https://repo1.dsop.io/platform-one/distros/vmware/tkg.git
```

There is a template in the templates folder named aws-airgap-mgmt-config.tpl.  Make a copy of the file in the root of the project with a name that makes sense to you.

```bash
cp templates/aws-airgap-mgmt-config.tpl aws-airgap-mgmt-config-test1
```

Work through the configuration items and set them based on your environment.  If you want a working configuration file, check out the `aws-tkg-env-example' file, the network CIDRs are tested and pre-configured.

| Variable | Description  |
| ------ | ------ |
|PLAN | dev-airgap, prod-airgap, oidc-airgap |
|VERBOSE | numerical value to control log verbosity  |
|MGMT_CLUSTER_NAME     | name of management cluster, required     |
|AWS_REGION     | aws region e.g. us-east-1, required   |
|AWS_NODE_AZ    | aws az e.g. us-east-1a, required   |
|AWS_PRIVATE_SUBNET_ID   | ID of first private deployment subnet, set by terraform if not bring your own infra.   |
|AWS_PRIVATE_NODE_CIDR   | CIDR of first subnet, required     |
|AWS_PRIVATE_SUBNET_ID_2    | ID of second private deployment subnet, set by terraform if not bring your own infra.      |
|AWS_PRIVATE_NODE_CIDR_2   | CIDR of second subnet, required   |
|AWS_SSH_KEY_NAME  | name of existing AWS key pair that you have the private key for, required     |
|BASTION_HOST_ENABLED | true/false, should a bastion host be created in air gapped VPC, likely false since we have a jumpbox, required     |
|CLUSTER_CIDR    | Overall cluster CIDR, required    |
|CONTROL_PLANE_MACHINE_TYPE   | EC2 machine type     |
|NODE_MACHINE_TYPE    | EC2 machine type     |
|AWS_B64ENCODED_CREDENTIALS    | Base64 credentials defined in later step, required     |
|AIRGAPPED_B64ENCODED_CA | Base64 encoded CA pem encoded CA cert, required if bring your own registry|
|TKG_CUSTOM_IMAGE_REPOSITORY | this can be the IP address of your private jumpbox (defined later) or if you bring your own registry that hostname, required|
### Bring Your Own VPC
Here is an example of a bring your own VPC configuration.  Note that the subnet and VPC IDs are pre-populated.

```bash
#AWS Var section
export PLAN=dev-airgap
export VERBOSE=100
export MGMT_CLUSTER_NAME=air-gapped-mgmt1

export VPC_NAME=tkg-demo2
export IAAS=aws
export AWS_REGION=us-east-1
export AWS_NODE_AZ=us-east-1a
export AWS_VPC_ID=vpc-036a58414e2859af7
export AWS_PRIVATE_SUBNET_ID=subnet-0f451f49b8f4420d4
export AWS_PRIVATE_SUBNET_ID_2=subnet-0fb48e9d53c62f1e5
export AWS_SSH_KEY_NAME=ag-tkg
export AWS_VPC_ID=vpc-036a58414e2859af7
export AWS_VPC_CIDR=10.0.0.0/16
export AWS_PRIVATE_NODE_CIDR=10.0.50.0/24
export AWS_PRIVATE_NODE_CIDR_2=10.0.52.0/24
export BASTION_HOST_ENABLED=false
export CLUSTER_CIDR=100.96.0.0/11
export CONTROL_PLANE_MACHINE_TYPE=m5.xlarge
export NODE_MACHINE_TYPE=m5.xlarge
export TKG_CUSTOM_IMAGE_REPOSITORY=registry.tkg-demo.info:5000
export AWS_B64ENCODED_CREDENTIALS=<base 64 encoded creds from first step>
export AIRGAPPED_B64ENCODED_CA=<base 64 encoded ca certs>

#VAULT Section
export VAULT_ENABLED=false
export VAULT_ADDR=
```

### Use Terraform to Create an Environment
With Terraform, just set the CIDR's as desired, the subnets and VPC IDs will be set by Terraform.

```bash
#AWS Var section
export PLAN=dev-airgap
export VERBOSE=100
export MGMT_CLUSTER_NAME=air-gapped-mgmt1

export VPC_NAME=tkg-demo2
export IAAS=aws
export AWS_REGION=us-east-1
export AWS_NODE_AZ=us-east-1a
export AWS_PRIVATE_SUBNET_ID=
export AWS_PRIVATE_SUBNET_ID_2=
export AWS_SSH_KEY_NAME=ag-tkg
export AWS_VPC_ID=
export AWS_VPC_CIDR=10.0.0.0/16
export AWS_PRIVATE_NODE_CIDR=10.0.50.0/24
export AWS_PRIVATE_NODE_CIDR_2=10.0.52.0/24
export BASTION_HOST_ENABLED=false
export CLUSTER_CIDR=100.96.0.0/11
export CONTROL_PLANE_MACHINE_TYPE=m5.xlarge
export NODE_MACHINE_TYPE=m5.xlarge
export TKG_CUSTOM_IMAGE_REPOSITORY=registry.tkg-demo.info
export AWS_B64ENCODED_CREDENTIALS=<base 64 encoded creds from first step>
export AIRGAPPED_B64ENCODED_CA=<base 64 encoded ca certs>

#VAULT Section
export VAULT_ENABLED=false
export VAULT_ADDR=

```
## Running Terraform

You only need to run Terraform if you want to create a VPC and subnets to try out TKG.  For users with existing infrastructure, you can skip this section.

This has been tested with Terraform v0.12.21 and higher.  You can download and install Terraform using these instructions:
https://learn.hashicorp.com/tutorials/terraform/install-cli

You will need to configure your AWS credential before running Terraform.  Run the following and provide credentials as prompted.  Make sure that you do not have any other credentials set in your environment that may interfere.

```bash
aws configure
```

We also filled out our configuration and added CIDR's for our 2 private subnets and related infrastructure.

Now we will create our airgapped VPC and jumpboxes by running



```bash
./create-tkg-aws-env.sh <your config file here>
```

Terraform will take some time to run. Upon completion Terraform will set the VPC and Subnet ID values in your configuration file. 

```bash
#AWS Var section
export PLAN=dev-airgap
export VERBOSE=100
export MGMT_CLUSTER_NAME=air-gapped-mgmt1

export VPC_NAME=tkg-test-t1
export IAAS=aws
export AWS_REGION=us-east-1
export AWS_NODE_AZ=us-east-1a
#Note these values get populated
export AWS_PRIVATE_SUBNET_ID=subnet-0230964eb1d6e3011
export AWS_PRIVATE_SUBNET_ID_2=subnet-0230964eb1d6e3011
export AWS_SSH_KEY_NAME=ag-tkg
#Note these values get populated
export AWS_VPC_ID=vpc-06b02a7b977ee9103
export AWS_VPC_CIDR=10.0.0.0/16
export AWS_PRIVATE_NODE_CIDR=10.0.53.0/24
export AWS_PRIVATE_NODE_CIDR_2=10.0.54.0/24
export BASTION_HOST_ENABLED=false
export CLUSTER_CIDR=100.96.0.0/11
export CONTROL_PLANE_MACHINE_TYPE=m5.xlarge
export NODE_MACHINE_TYPE=m5.xlarge
export TKG_CUSTOM_IMAGE_REPOSITORY=test1-registry.tkg-demo.info
export AWS_B64ENCODED_CREDENTIALS=
export AIRGAPPED_B64ENCODED_CA=

#VAULT Section
export VAULT_ENABLED=false
export VAULT_ADDR=
```

When Terraform completes it will print the IP addresses of your jumpboxes, take note of these

### Take Note of your Jumpbox IP Addresses

```
Please take note of your connected jump boxes IP address info
>>>>>>>>>>>> [ "18.215.118.185", ]
You will also need to know the ip address of your airgapped jump box
>>>>>>>>>>>> [ "10.0.53.220", ]

```

The air gapped jumpbox ip can be used to populate the TKG_CUSTOM_IMAGE_REPOSITORY value of your config file if you do not plan to use your own image registry.

## Setting IAM Profile and User Credentials for TKG

Regardless of your deployment path, you will need to create cluster api bootstrapper user and capture the base64 encoded credentials produced in the last step of this sequence. 

You have already downloaded clusterawsadm, make sure that is on your path and executable before you proceed.

```bash
export AWS_ACCESS_KEY_ID=aws_access_key
export AWS_SECRET_ACCESS_KEY=aws_access_key_secret
# if needed 
export AWS_SESSION_TOKEN=aws_session_token

export AWS_REGION=uaws_region

clusterawsadm alpha bootstrap create-stack

export AWS_CREDENTIALS=$(aws iam create-access-key --user-name bootstrapper.cluster-api-provider-aws.sigs.k8s.io --output json)

# Replace the environment variable that you created for your AWS access key ID.

export AWS_ACCESS_KEY_ID=$(echo $AWS_CREDENTIALS | jq .AccessKey.AccessKeyId -r)

# Replace the environment variable that you created for your secret access key.

export AWS_SECRET_ACCESS_KEY=$(echo $AWS_CREDENTIALS | jq .AccessKey.SecretAccessKey -r)

export AWS_B64ENCODED_CREDENTIALS=$(clusterawsadm alpha bootstrap encode-aws-credentials)

```

Take the output stored in AWS_B64ENCODED_CREDENTIALS and insert it into your config file.

```bash
...
export TKG_CUSTOM_IMAGE_REPOSITORY=test1-registry.tkg-demo.info
export AWS_B64ENCODED_CREDENTIALS=<paste in base64 here>
export AIRGAPPED_B64ENCODED_CA=
...
```


## Staging your Deployment on the Connected Jumpbox


### Make sure you have the TKG CLI

Ensure that you have the TKG CLI binary downloaded and added into the root of your project folder before deploying the project to AWS.  There are instructions for downloading the CLI and TKG Extensions in the [README](../README.md).

At this point if you are using terraform you have created a internet accessible jumpbox.  We will now deploy our project there.  If you have an existing VPC and jumpbox or another way accessing your airgapped environment you can skip this step.

```bash
./depoloy-env.sh <public ip of jump> <path to key for jumpbox>
```

The IP address was printed out by Terraform and an AWS key pair was specified in the AWS_SSH_KEY_NAME field.

Lets ssh into our jumpbox now

```bash
ssh -i <path to key> ubuntu@<ip of public jump box>
```

We will now install the dependnecies we need to proceed with the install.  Note this has been tested with the Terraform based install.

```bash
./install-connected-depends.sh 
```

For those that have their own jump box, the following components are installed.

- tkg == v1.1.3 cli in $PATH
- jq >= 1.6 cli in $PATH
- yq >= 3.4.0 cli in $PATH
- docker >= 18.09.7 in $PATH

We need to initiliaze the TKG CLI on our public jump box in order to get a list of the required container images.  You should have added this into your project folder before it was deployed.

```bsash
chmod +x <CLI binary>
sudo cp <CLI binary>  /usr/local/bin/tkg
```

## Preparing Install Images

We need to bundle all of the required container images for the air gapped install.  If you already have a registry in your environment, your path may stray here.  We have included a basic registry image that can be used as a temporary starting point.

We need to initiliaze the TKG CLI in order to get a list of the required container images.  

```bsash
tkg get mc
```

### Take Note When Selecting your Image Registry

Make sure you have set the value of TKG_CUSTOM_IMAGE_REPOSITORY in your config file.  This can be the ip of your air gapped jumpbox if you do not already have one.  Otherwise, make sure your existing container registry is accessible from within the air gapped VPC.

```bash
source <the config file you setup earlier with TKG_CUSTOM_IMAGE_REPOSITORY>

#to confirm
echo $TKG_CUSTOM_IMAGE_REPOSITORY
```

Now we are going to build up the bundle of required images by running:

```bash
./registry/gen-publish-images-airgap.sh
```
This script will output a path to another script called pull-imags.sh. Upon running, you should be seeing images pulled down one by one.  This script will take ~10 mins to run, take a break.

```bash
#Pull image script file created: /home/ubuntu/pull-images.sh

sudo /home/ubuntu/pull-images.sh
```

You should see `Processing image 93 of 93` at the end of the script.  Now we are going to tar the images up.

```bash
sudo tar -zcf ~/tkg-images-$(tkg version -o json | jq -r '.Version').tgz tkg-images-$(tkg version -o json | jq -r '.Version')

#once tar completes delete the folder
sudo rm -Rf tkg-images-v1.1.3
```

We now have all our images bundled in `tkg-images-v1.1.3.tgz'. Now we are going to move over to our air gapped environment.  Recall that we got the private ip of the air gapped jumpbox from Terraform.

```Bash
./deploy-env.sh <private ip of airgapped jumpbox> <private key for jumpbox>

ssh -i <private key for jumpbox> ubuntu@<private ip of airgapped jumpbox>
```

## Standing Up an Install Registry

We have now deployed our project to the air gapped environment.  

Below are instructions for setting up a temporary registry for our install.  If you already have a registry, please check out the CA cert configuration below, but you will not need to run through the following steps.

```bash
source <config file>

#this will start docker
./install-ag-depends.sh

#hit enter to proceed, this script can hang at end

#load the registry image into docker
sudo docker load -i registry/registry-image.tar

#run the registry
sudo docker run -d -p 80:5000 --restart=always --name registry registry:2

#confirm the container is running 
sudo docker ps
```
We now have a working container registry.  This is just meant to support the install, so we are keeping it very simple.

### CA Cert Considerations

You will need to follow the appropriate secion based on whether you are using your own Registry or used the simple install Registry we have provided.

#### Bring your Own Registry
In addition to DNS, you will also need to configure CA certs with your existing registry.  If your environment has an existing container registry that is configured with certificates, place the CA certificate (or chain) in a PEM format at `kind/registry-ca.crt' within the project.  This will ensure that your cert gets included in the resulting cluster.

```bash
#from the project root

#Set the right template for a CA cert
cp templates/containerd-config-ca.toml templates/containerd-config.toml

#confirm your cert is there
cat kind/registry-ca.crt
```

#### Using the Temporary Install Registry
If you use the simple install registry provided within the project, you can skip adding CA certs.  We do need to add an exclusion to our containerd configuration for the install registry.  See the configuration below.

```bash
#from the project root

#Set the right template for an insecure registry
cp templates/containerd-insecure.toml templates/containerd-config.toml 

#init the TKG CLI
chmod +x <CLI binary>
sudo cp <CLI binary>  /usr/local/bin/tkg

tkg get mc
```

Add the "containerdConfigPatches" section into your ~/.tkg/bom/bom-1.1.3+vmware.1.yaml file. You will find the existing "kindKubeadmConfigSpec" within the file.  Populate your registry name into the place holder values <TKG_CUSTOM_IMAGE_REPOSITORY>.  The Registry name will be the value of TKG_CUSTOM_IMAGE_REPOSITORY you selected above for your config file.

```yaml

#With your registry name populated in the placeholders sections below
...
kindKubeadmConfigSpec:
- 'kind: Cluster'
- 'apiVersion: kind.x-k8s.io/v1alpha4'
#################copy and paste this section into ~/.tkg/bom/bom-1.1.3+vmware.1.yaml###########################
- 'containerdConfigPatches:'
- '- |-'
- '  [plugins."io.containerd.grpc.v1.cri".registry.mirrors."<TKG_CUSTOM_IMAGE_REPOSITORY>"]'
- '    endpoint = ["http://<TKG_CUSTOM_IMAGE_REPOSITORY>"]'
###############################################################################################################
- 'kubeadmConfigPatches:'
- '- |'
- '  apiVersion: kubeadm.k8s.io/v1beta2'
- '  kind: ClusterConfiguration'
- '  imageRepository: registry.tkg.vmware.run'
- '  etcd:'
...
```

With our registry configured we need to push our image bundle into the registry.


```bash
#untar the images
tar xvf tkg-images-v1.1.3.tgz

cd tkg-images-v1.1.3/

#push the images into our new repo, this will take ~10 mins
./push-images.sh ${TKG_CUSTOM_IMAGE_REPOSITORY}
```

You should see `Processing image 93 of 93` at the end of the script execution.


## Creating a Management Cluster

We are now ready to proceed with creating a management cluster.  If you are following the full deployment Terraform populated the Subnet and VPC IDs in your config file.  If you brought your own VPC, check the config file and make sure that it is filled out completely.

```bash
#create the cluster
sudo ./deploy-mgmt-cluster.sh <your config file>

#validate cluster
kubectl get all
```

You now have a working Management Cluster.  You can proceed to create and manage workload clusters.  Please checkout some of the notes about managing clusters in an air gapped environment [here](mgmt.md).

