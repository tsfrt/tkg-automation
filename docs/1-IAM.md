# Setting IAM Profile and User Credentials

This first step is only needed if you will be using Terraform. If you do not need to run Terraform, you can skip this.  You will need an account that can create VPCs, Subnets, EC2 instances, adding endpoints, adding routes, and creating peering connections.

```bash
#use your new Access key and Secret as prompted
aws configure

```

Regardless of your deployment path, you will need to create cluster api bootstrapper user and capture the base64 encoded credentials produced in the last step of this sequence.  If you will be running the full deployment you will need to run these steps from your local machine ahead of time to run Terraform.

```bash
export AWS_ACCESS_KEY_ID=aws_access_key
export AWS_SECRET_ACCESS_KEY=aws_access_key_secret
# if needed 
export AWS_SESSION_TOKEN=aws_session_token

export AWS_REGION=uaws_region

clusterawsadm alpha bootstrap create-stack

export AWS_CREDENTIALS=$(aws iam create-access-key --user-name bootstrapper.cluster-api-provider-aws.sigs.k8s.io --output json)

# Replace the environment variable that you created for your AWS access key ID.

export AWS_ACCESS_KEY_ID=$(echo $AWS_CREDENTIALS | jq .AccessKey.AccessKeyId -r)

# Replace the environment variable that you created for your secret access key.

export AWS_SECRET_ACCESS_KEY=$(echo $AWS_CREDENTIALS | jq .AccessKey.SecretAccessKey -r)

export AWS_B64ENCODED_CREDENTIALS=$(clusterawsadm alpha bootstrap encode-aws-credentials)

```

Then save your base64 encoded credentials for later as you will need them to run the TKG cli and when you fill out your config.

