# Running Terraform

You only need to run Terraform if you want to create a VPC and subnets to try out TKG.  For users with existing infrastructure, you can skip this section.

This has been tested with Terraform v0.12.21 and higher.  You can download and install Terraform using these instructions:
https://learn.hashicorp.com/tutorials/terraform/install-cli

In the last step you ran:

```bash
aws configure
```

This configured an admin level user for Terraform.  

We also filled out our configuration and added CIDR's for our 2 private subnets and related infrastructure.

Now we will create our airgapped VPC and jumpboxes by running



```bash

./create-tkg-aws-env.sh <your config file here>

```

Terraform will take some time to run. Upon completion Terraform will set the VPC and Subnet ID values in your configuration file. 

```bash

export VPC_NAME=tkg-test-t1
export IAAS=aws
export AWS_REGION=us-east-1
export AWS_NODE_AZ=us-east-1a
#Note these values get populated
export AWS_PRIVATE_SUBNET_ID=subnet-0230964eb1d6e3011
export AWS_PRIVATE_SUBNET_ID_2=subnet-0230964eb1d6e3011
export AWS_SSH_KEY_NAME=ag-tkg
#Note these values get populated
export AWS_VPC_ID=vpc-06b02a7b977ee9103
export AWS_VPC_CIDR=10.0.0.0/16
export AWS_PRIVATE_NODE_CIDR=10.0.53.0/24
export AWS_PRIVATE_NODE_CIDR_2=10.0.54.0/24
export BASTION_HOST_ENABLED=false
export CLUSTER_CIDR=100.96.0.0/11
export CONTROL_PLANE_MACHINE_TYPE=m5.xlarge
export NODE_MACHINE_TYPE=m5.xlarge
export TKG_CUSTOM_IMAGE_REPOSITORY=test1-registry.tkg-demo.info
export AWS_B64ENCODED_CREDENTIALS=AWS_B64ENCODED_CREDENTIALS=W2RlZmF1bHRdCmF3c19hY2Nlc3Nfa2V5X2lkID0gQUtJQTZKNkIzTk9BVE9QSEdIMlIKYXdzX3NlY3JldF9hY2Nlc3Nfa2V5ID0gV0V5Zk44NVFML1ovSzE0cHI1aHZsSlhiUGNwcVBnTlJqMEo5SXdBNApyZWdpb24gPSB1cy1lYXN0LTEKCg==
export AIRGAPPED_B64ENCODED_CA=dGhpcyBpcyBhIGR1bW15Cg==

#VAULT Section
export VAULT_ENABLED=false
export VAULT_ADDR=

```

When Terraform completes it will print the IP addresses of your jumpboxes, take note of these

```

Please take note of your connected jump boxes IP address info
>>>>>>>>>>>> [ "18.215.118.185", ]
You will also need to know the ip address of your airgapped jump box
>>>>>>>>>>>> [ "10.0.53.220", ]

```

