# Creating a Management Cluster

We are now ready to proceed with creating a management cluster.  If you are following the full deployment Terraform populated the Subnet and VPC IDs in your config file.  If you brought your own VPC, check the config file and make sure that it is filled out completely.

```bash
#create the cluster
sudo ./deploy-mgmt-cluster.sh <your config file>

#validate cluster
kubectl get all

```

You now have a working Management Cluster.  You can proceed to create and manage workload clusters.