# Staging your Deployment on the Connected Jumpbox

Ensure that you have the TKG CLI binary downloaded and added into the root of your project folder before deploying the project to AWS.  This is downloaded seperately.

At this point if you are using terraform you have created a internet accessible jumpbox.  We will now deploy our project there.  If you have an existing VPC and jumpbox or another way accessing your airgapped environment you can skip this step.

```bash
./depoloy-env.sh <public ip of jump> <path to key for jumpbox>

```

The IP address was printed out by Terraform and an AWS key pair was specified in the AWS_SSH_KEY_NAME field.

Let ssh into our jumpbox now

```bash
ssh -i <path to key> ubuntu@<ip of public jump box>

```

We will now install the dependnecies we need to proceed with the install.  Note this has been tested with the Terraform based install.

```bash
./install-connected-depends.sh 
```

For those that have their own jump box, the following components are installed.

- tkg == v1.1.3 cli in $PATH
- jq >= 1.6 cli in $PATH
- yq >= 3.4.0 cli in $PATH
- docker >= 18.09.7 in $PATH

We need to initiliaze the TKG CLI on our public jump box in order to get a list of the required container images.  You should have added this into your project folder before it was deployed.

```bsash
chmod +x <CLI binary>
sudo cp <CLI binary>  /usr/local/bin/tkg
```

