#!/bin/bash
WORKING_DIR=$(dirname "$0")
install_dir=$WORKING_DIR/tkg-install-dependencies

chmod +x $install_dir/yq
sudo cp $install_dir/yq /usr/local/bin/yq

chmod +x $install_dir/jq
sudo cp $install_dir/jq /usr/local/bin/jq

chmod +x $install_dir/clusterawsadm-linux-amd64
sudo cp $install_dir/clusterawsadm-linux-amd64 /usr/local/bin/clusterawsadm