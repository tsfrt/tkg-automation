#!/bin/bash

WORKING_DIR=$(dirname "$0")
install_dir=$WORKING_DIR/tkg-install-dependencies

sudo apt update
sudo apt --assume-yes install awscli
sudo apt --assume-yes install unzip
sudo apt --assume-yes install docker.io

unzip $install_dir/awscliv2.zip
sudo ./aws/install

