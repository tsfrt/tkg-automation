#!/bin/bash
WORKING_DIR=$(dirname "$0")
install_dir=$WORKING_DIR/tkg-install-dependencies


sudo tar xfz $install_dir/docker-install -C $install_dir
sudo cp $install_dir/docker/* /usr/local/bin/
sudo mkdir -p /etc/docker
if [ $AWS_PRIVATE_DNS -z ]; then
  echo "{\"insecure-registries\": [ \"${TKG_CUSTOM_IMAGE_REPOSITORY}\" ]}" | sudo tee /etc/docker/daemon.json
else
  echo "{\"insecure-registries\": [ \"${TKG_CUSTOM_IMAGE_REPOSITORY}\" ], \"dns\": [\"${AWS_PRIVATE_DNS}\"]}" | sudo tee /etc/docker/daemon.json
fi
sudo dockerd &

chmod +x $install_dir/kubectl
sudo cp $install_dir/kubectl /usr/local/bin/kubectl

#Docker is only used to support install
#this jumpbox is disposible
sudo chmod 777 /var/run/docker.sock