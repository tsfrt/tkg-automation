#!/bin/bash

WORKING_DIR=$(dirname "$0")
install_dir=$WORKING_DIR/tkg-install-dependencies
mkdir $install_dir

curl -o $install_dir/clusterawsadm-linux-amd64 -L https://github.com/kubernetes-sigs/cluster-api-provider-aws/releases/download/v0.6.1/clusterawsadm-linux-amd64

curl -o $install_dir/yq -L https://github.com/mikefarah/yq/releases/download/3.4.1/yq_linux_amd64

curl -o $install_dir/jq -L https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64

curl -o $install_dir/docker-install -L https://download.docker.com/linux/static/stable/x86_64/docker-18.06.3-ce.tgz

curl -o $install_dir/kubectl -L https://storage.googleapis.com/kubernetes-release/release/v1.19.0/bin/linux/amd64/kubectl

curl  -o $install_dir/"awscliv2.zip" -L "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip"