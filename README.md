 <img src="https://d1fto35gcfffzn.cloudfront.net/tanzu/tanzu-bug.svg" width="200" height="200" /> 
 
# Tanzu Kubernetes Grid For Air Gapped Environments

This repo provides a hands-on guide for to get started Tanzu Kubernetes Grid.  This guide illustrates a example implementation of Tanzu Kubernetes Grid in an air gapped environment.  It is intended as a starting place for those interested in deploying and managing open source Kubernetes clusters using TKG’s Cluster API based control plane.  This is a work in progress to provide a starting point to learn and try out TKG.

## Deployment Options

There are several different deployment options that can be followed using this guide:

-	Full Air Gapped Deployment – run terraform to create an air gapped VPV and public facing jumpbox that can reach a jumpbox on the private VPC.  This will require you to install terraform on your local machine and run through the environment creation before moving to the public jumpbox.
-	Bring your own Infrastructure Air Gapped Deployment – If you already have your own air gapped VPC and a mechanism for moving assets into your air gapped environment you can use this path to create a TKG Management Cluster in your existing VPC.
-	Standard/Non-Air Gapped Deployment – If you have access to running a standard internet connected TKG install, this option will likely be the easiest way to get started and evaluate TKG with fewer installation requirements.  Download the latest TKG binary and follow the standard install instructions published at:
https://docs.vmware.com/en/VMware-Tanzu-Kubernetes-Grid/1.1/vmware-tanzu-kubernetes-grid-11/GUID-install-tkg-index.html


## Prerequisites
1.  You will need to gain access to the TKG CLI Binary in order to run the TKG.

2. If you are bringing your own infrastructure, you need to have access to a container registry and its CA certificates. This registry must be accessible within your air gapped VPC.

3. You will need to have privileges in AWS to create VPCs, Sunbets, Internet Gateways, NAT Gateways, etc.  

4. The following components must be installed on your jumpbox(s) and are referenced in the instructions.  If you follow the full deployment, these tools will be installed as part of the process:

- awscli (local install required if using Terraform)
- clusterawsadm (Required, download along with TKG CLI)
- Terraform tested with Terraform v0.12.21 (local install required if using Terraform)
(https://learn.hashicorp.com/tutorials/terraform/install-cli) 
- tkg == v1.1.3 cli in $PATH (Required Download)
- jq >= 1.6 cli in $PATH (Installed automatically if following Full deployment with Terraform)
- yq >= 3.4.0 cli in $PATH (Installed automatically if following Full deployment with Terraform)
- docker >= 18.09.7 in $PATH (Installed automatically if following Full deployment with Terraform)
 
## Getting the TKG CLI
The TKG CLI Binary is required to run the installation process.  The CLI can be downloaded as follows:

If you do not already have an account, register at https://my.vmware.com (look at the top right).  Once registered login and navigate with the link below.

Grab the correct CLI for your environment (mac, windows, linux) if you want to run a non-air gapped deployment. The Terraform based deployment process requires the Linux binary.  

Download the following files from the [TKG CLI Download Page](https://my.vmware.com/web/vmware/downloads/details?downloadGroup=TKG-113&productId=988):
- clusterawsadm
- VMware Tanzu Kubernetes Grid Extensions Manifest 1.1
- VMware Tanzu Kubernetes Grid CLI for Linux (or your desired OS)

Put both the CLI and Extensions in your project folder once you have cloned it.

## Installation
As we work through the install process, we will walk through the full deployment path and point out where this approach can be used to deploy a partial deployment using your own AWS infrastructure and/or container registry. 

[here](docs/install.md)


## Cluster Provisioning and Management
Use your TKG Management cluster to create workload clusters, apply changes, updates or scale using the TKG CLI or Kubeclt directly.

[here](docs/mgmt.md)

https://docs.vmware.com/en/VMware-Tanzu-Kubernetes-Grid/1.1/vmware-tanzu-kubernetes-grid-11/GUID-tanzu-k8s-clusters-index.html

## Next Steps
Use the VMware Tanzu Kubernetes Grid Extensions Manifest 1.1 package to further productionize your deployment.

- Setup User Authentication
- Log Forwarding
- Ingress

https://docs.vmware.com/en/VMware-Tanzu-Kubernetes-Grid/1.1/vmware-tanzu-kubernetes-grid-11/GUID-manage-instance-index.html
